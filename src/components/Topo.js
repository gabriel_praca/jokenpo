
import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

// const dimensions = Dimensions.get('window');
// const imageWidth = dimensions.width;

const logo = require('../../imgs/jokenpo.png');

export default class Topo extends React.Component {
    render() {
        return (
            <View style={styles.img}>
                <Image style={styles.img} source={logo} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    img: {
        width: '100%',
        backgroundColor: 'cyan'
    }
});
