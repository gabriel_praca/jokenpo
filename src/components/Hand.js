
import React from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';

const rockImg = require('../../imgs/pedra.png');
const paperImg = require('../../imgs/pedra.png');
const scissorImg = require('../../imgs/pedra.png');

export default class Hand extends React.Component {
    render() {        
        if (this.props.choice === 'pedra') {
            return (
                <View style={styles.hand}>
                    <Text style={styles.txtPlayer}> {this.props.player} </Text>
                    <Image source={rockImg} />
                </View>
            );
        } else if (this.props.choice === 'papel') {
            return (
                <View style={styles.hand}>
                    <Text style={styles.txtPlayer}> {this.props.player} </Text>
                    <Image source={paperImg} />
                </View>
            );
        } else if (this.props.choice === 'tesoura') {
            return (
                <View style={styles.hand}>
                    <Text style={styles.txtPlayer}> {this.props.player} </Text>
                    <Image source={scissorImg} />
                </View>
            );
        }        
        return false;
    }
}

const styles = StyleSheet.create({
    hand: {
        alignItems: 'center',
        marginVertical: 15
    },
    txtPlayer: {
        fontSize: 18
    }
});
