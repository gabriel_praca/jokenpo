
import React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';
import Topo from './src/components/Topo.js';
import Hand from './src/components/Hand.js';

export default class main extends React.Component {
    constructor(props) {
    super(props);

    this.state = { 
                    userChoice: '',
                    comChoice: '',
                    winner: ''
                };
  }

jokenpo(userChoice) {
    const numChoice = Math.floor(Math.random() * 3);
    let comChoice = '';
    let result = '';

    switch (numChoice) {
        case 0 : 
            comChoice = 'pedra';
            break;
        case 1 :
            comChoice = 'papel';
            break;
        case 2:
            comChoice = 'tesoura';
            break;
        default :
            break;
    }

    if (comChoice === 'pedra') {
        if (userChoice === 'pedra') {
            result = 'Empate';
        }
        if (userChoice === 'papel') {
            result = 'Você venceu';
        }
        if (userChoice === 'tesoura') {
            result = 'Você perdeu';
        }
    } else if (comChoice === 'papel') {
        if (userChoice === 'pedra') {
            result = 'Você perdeu';
        }
        if (userChoice === 'papel') {
            result = 'Empate';
        }
        if (userChoice === 'tesoura') {
            result = 'Você venceu';
        }
    } else if (comChoice === 'tesoura') {
        if (userChoice === 'pedra') {
            result = 'Você venceu';
        }
        if (userChoice === 'papel') {
            result = 'Você perdeu';
        }
        if (userChoice === 'tesoura') {
            result = 'Empate';
        }
    }

    this.setState({ userChoice });
    this.setState({ comChoice });
    this.setState({ winner: result });
}

    render() {
        return (
            <View>
                <Topo />

                <View style={styles.painelButtons}>
                    <View styles={styles.btnChoice}>
                        <Button title='Pedra' onPress={() => { this.jokenpo('pedra'); }} />
                    </View>
                    <View style={styles.btnChoice}>
                        <Button title='Papel' onPress={() => { this.jokenpo('papel'); }} />
                    </View>
                    <View styles={styles.btnChoice}>
                        <Button title='Tesoura' onPress={() => { this.jokenpo('tesoura'); }} />
                    </View>
                </View>

                <View style={styles.palco}>
                    <Text style={styles.txtRes}> {this.state.winner} </Text>

                    <Hand choice={this.state.comChoice} player='Computador' />
                    <Hand choice={this.state.userChoice} player='Você' />

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    btnChoice: {
        width: 150
    },
    painelButtons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 10,
    },
    palco: {
        alignItems: 'center',
        marginTop: 10,
    },
    txtRes: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'red',
        height: 50
    },
});
